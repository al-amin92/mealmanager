package net.therap.mealmanager.Connector;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author al-amin
 * @since 11/14/16
 */
public class Database {
    public Connection connect() throws IOException {
        Connection connection = null;

        Properties prop = new Properties();
        InputStream input = new FileInputStream("mealmanager.properties");
        prop.load(input);

        System.out.println("-------- MySQL JDBC Connection Testing ------------");

        try {
            Class.forName(prop.getProperty("drivername"));
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your MySQL JDBC Driver?");
            e.printStackTrace();
            return connection;
        }

        System.out.println("MySQL JDBC Driver Registered!");

        try {
            connection = DriverManager
                    .getConnection(prop.getProperty("database"),prop.getProperty("username") ,prop.getProperty("password") );

        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return connection;
        }

        if (connection != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }
        return connection;
    }
}
