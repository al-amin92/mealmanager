package net.therap.mealmanager.Controller;

import java.io.IOException;
import java.sql.SQLException;

/**
 * @author al-amin
 * @since 11/14/16
 */
public class Main {
    public static void main(String[] args) throws SQLException, NoSuchFieldException, IOException {
        UserInteraction userInteraction = new UserInteraction();
        userInteraction.interact();
    }
}
