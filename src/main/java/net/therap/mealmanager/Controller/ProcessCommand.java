package net.therap.mealmanager.Controller;

import java.io.IOException;
import java.sql.SQLException;

/**
 * @author al-amin
 * @since 11/14/16
 */
public class ProcessCommand {
    private TakeAction takeAction;

    public ProcessCommand() throws SQLException, IOException {
        takeAction = new TakeAction();
    }

    public void process(String command) throws SQLException, NoSuchFieldException {
        if (command.length() == 0) {
            return;
        }
        String cmd = command.split(" ")[0];

        if (cmd.equals("help")) {
            takeAction.executeHelp();
        } else if (cmd.equals("show")) {
            takeAction.executeShow(command);
        } else if (cmd.equals("set")) {
            takeAction.executeSet(command);
        } else if (cmd.equals("delete")) {
            takeAction.executeDelete(command);
        } else if (cmd.equals("login")) {
            takeAction.executeLogin(command);
        } else if (cmd.equals("logout")) {
            takeAction.executeLogout();
        } else if (cmd.equals("insert")) {
            takeAction.executeInsert(command);
        }
    }
}
