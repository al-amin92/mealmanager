package net.therap.mealmanager.Controller;

import net.therap.mealmanager.DAO.DatabaseDAO;
import net.therap.mealmanager.Objects.FoodItems;
import net.therap.mealmanager.Objects.MealItems;
import net.therap.mealmanager.Objects.WeeklyMealPlan;
import net.therap.mealmanager.View.Show;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author al-amin
 * @since 11/14/16
 */
public class TakeAction {

    private DatabaseDAO databaseDAO;
    private Show show;

    public TakeAction() throws SQLException, IOException {
        databaseDAO = new DatabaseDAO();
        show = new Show();
    }

    public void executeHelp() {
        show.showHelp(databaseDAO.getAdminAccess());
    }

    public void executeShow(String command) throws SQLException, NoSuchFieldException {
        /**
         * Database View Operations
         */
        String cmd[] = command.split(" ");
        if (cmd[1].equals("items")) {
            List<FoodItems> foodItems = new ArrayList<>();
            foodItems = databaseDAO.query(new String[]{"food_items"});
            if (databaseDAO.getAdminAccess()) {
                show.showFoodItems(foodItems, new String[]{"item_id", "item_name", "item_cost"});
            } else {
                show.showFoodItems(foodItems, new String[]{"item_name"});
            }
        } else if (cmd[1].equals("meal")) {
            List<WeeklyMealPlan> weeklyMealPlan = new ArrayList<>();

            if (cmd.length == 2) {
                weeklyMealPlan = databaseDAO.query(new String[]{"weekly_meal_plan"});
            } else if (cmd.length == 3) {
                weeklyMealPlan = databaseDAO.query(new String[]{"weekly_meal_plan", cmd[2]});
            } else if (cmd.length == 4) {
                weeklyMealPlan = databaseDAO.query(new String[]{"weekly_meal_plan", cmd[2], cmd[3]});
            }
            for (WeeklyMealPlan entity : weeklyMealPlan) {
                List<MealItems> mealItems = databaseDAO.query(new String[]{"meal_items",
                        Integer.toString(entity.getMeal_id())});

                show.printLine(entity.getWeek_day() + " " + entity.getMeal_type() + ": ");
                show.showMealItems(mealItems, new String[]{"item_id", "item_name"});
                if (databaseDAO.getAdminAccess()) {
                    show.printLine("per meal cost : " + entity.getPer_meal_cost());
                }
                show.printLine("");
            }
        }
    }

    public void executeSet(String command) throws SQLException {
        /**
         * Database Update Operations
         */
        if (!databaseDAO.getAdminAccess()) {
            show.printLine("access denied. please log in as admin.");
            return;
        }
        String cmd[] = command.split(" ");
        if (cmd[1].equals("meal") && cmd.length > 4) {
            String rootQuery = "select meal_id from meal_planner.weekly_meal_plan";
            String rootQuery2 = "select item_name,item_cost from meal_planner.food_items";
            String prefixDay = cmd[2].substring(0, 3).toLowerCase();
            String prefixTime = cmd[3].toLowerCase();

            ResultSet resultSet = databaseDAO.query(rootQuery +
                    " where week_day like '" + prefixDay + "%' and meal_type like '" + prefixTime + "'");
            resultSet.next();

            int meal_id = resultSet.getInt("meal_id");

            databaseDAO.delete("delete from meal_planner.meal_items  where meal_id=" + meal_id);
            int totalCost = 0;
            for (int i = 4; i < cmd.length; i++) {
                ResultSet resultSet3 = databaseDAO.query(rootQuery2 +
                        " where item_id=" + cmd[i]);
                resultSet3.next();
                String item_name = resultSet3.getString("item_name");
                int item_cost = resultSet3.getInt("item_cost");
                totalCost += item_cost;

                databaseDAO.insert("insert into meal_planner.meal_items  values(" + meal_id + ", " + cmd[i] +
                        ", '" + item_name + "')");
            }

            databaseDAO.update("update meal_planner.weekly_meal_plan set per_meal_cost=" + totalCost + " where" +
                    " meal_id=" + meal_id);

        }
    }

    public void executeLogin(String command) throws SQLException {
        /**
         * Admin Login
         */
        if (databaseDAO.getAdminAccess()) {
            System.out.println("you are already logged in");
        }
        String cmd[] = command.split(" ");
        if (cmd.length == 3) {
            String rootQuery = "select admin_password from admin_panel";
            String username = cmd[1];
            String password = cmd[2];
            ResultSet resultSet = databaseDAO.query(rootQuery + " where admin_username" + " like '" + username + "'");

            if (resultSet.next() && resultSet.getString("admin_password").equals(password)) {
                show.printLine("logged in as " + username);
                databaseDAO.setAdminAccess(true);
            } else {
                show.printLine("try again");
            }
        }
    }

    public void executeLogout() {
        /**
         * Admin Logout
         */
        if (!databaseDAO.getAdminAccess()) {
            show.printLine("you are already logged out");
        }
        databaseDAO.setAdminAccess(false);
        show.printLine("logged out");
    }

    public void executeInsert(String command) throws SQLException {
        /**
         * Database Insert Operations
         */
        if (!databaseDAO.getAdminAccess()) {
            show.printLine("access denied. please log in as admin.");
            return;
        }
        String cmd[] = command.split(" ");
        if (cmd.length == 4 && cmd[1].equals("item")) {
            ResultSet resultSet = databaseDAO.query("select max(item_id) as mx from meal_planner.food_items");
            resultSet.next();
            int item_id = resultSet.getInt("mx") + 1;
            databaseDAO.insert("insert into meal_planner.food_items value (" + item_id + ", '"
                    + cmd[2] + "', " + cmd[3] + ")");
        } else if (cmd[1].equals("meal") && cmd.length > 4) {
            String rootQuery = "select meal_id,per_meal_cost from meal_planner.weekly_meal_plan";
            String rootQuery2 = "select item_name,item_cost from meal_planner.food_items";

            String prefixDay = cmd[2].substring(0, 3).toLowerCase();
            String prefixTime = cmd[3].toLowerCase();

            ResultSet resultSet = databaseDAO.query(rootQuery + " where week_day like '" + prefixDay +
                    "%' and meal_type like '" + prefixTime + "'");
            resultSet.next();

            int meal_id = resultSet.getInt("meal_id");
            int per_meal_cost = resultSet.getInt("per_meal_cost");
            int extraCost = 0;

            for (int i = 4; i < cmd.length; i++) {

                ResultSet resultSet3 = databaseDAO.query(rootQuery2 + " where item_id=" + cmd[i]);
                resultSet3.next();
                String item_name = resultSet3.getString("item_name");
                int item_cost = resultSet3.getInt("item_cost");
                extraCost += item_cost;
                databaseDAO.insert("insert into meal_planner.meal_items  values(" + meal_id + ", " + cmd[i] + ", '"
                        + item_name + "')");
            }
            per_meal_cost += extraCost;
            databaseDAO.update("update meal_planner.weekly_meal_plan set per_meal_cost=" + per_meal_cost + " where" +
                    " meal_id=" + meal_id);
        }
    }

    public void executeDelete(String command) throws SQLException {
        /**
         * Database Delete and Update Operations
         */
        if (!databaseDAO.getAdminAccess()) {
            show.printLine("access denied. please log in as admin.");
            return;
        }
        String cmd[] = command.split(" ");
        if (cmd.length > 2 && cmd[1].equals("item")) {
            databaseDAO.delete("delete from meal_planner.food_items where item_id=" + cmd[2]);
        } else if (cmd[1].equals("meal") && cmd.length > 4) {
            String rootQuery = "select meal_id,per_meal_cost from meal_planner.weekly_meal_plan";
            String rootQuery2 = "select item_name,item_cost from meal_planner.food_items";

            String prefixDay = cmd[2].substring(0, 3).toLowerCase();
            String prefixTime = cmd[3].toLowerCase();

            ResultSet resultSet = databaseDAO.query(rootQuery + "  where week_day like '" + prefixDay +
                    "%' and meal_type like '" + prefixTime + "'");
            resultSet.next();

            int meal_id = resultSet.getInt("meal_id");
            int per_meal_cost = resultSet.getInt("per_meal_cost");
            int costDeduction = 0;

            for (int i = 4; i < cmd.length; i++) {

                ResultSet resultSet3 = databaseDAO.query(rootQuery2 + " where item_id=" + cmd[i]);
                resultSet3.next();
                String item_name = resultSet3.getString("item_name");
                int item_cost = resultSet3.getInt("item_cost");
                costDeduction += item_cost;
                databaseDAO.delete("delete from meal_planner.meal_items where meal_id=" + meal_id + " and item_id="
                        + cmd[i]);
            }
            per_meal_cost -= costDeduction;
            databaseDAO.update("update meal_planner.weekly_meal_plan set per_meal_cost=" + per_meal_cost +
                    " where meal_id=" + meal_id);
        }
    }
}
