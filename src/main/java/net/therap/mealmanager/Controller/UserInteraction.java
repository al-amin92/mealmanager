package net.therap.mealmanager.Controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author al-amin
 * @since 11/14/16
 */
public class UserInteraction {

    public void interact() throws SQLException, NoSuchFieldException, IOException {
        System.out.println("in iteract()");
        String command;
        ProcessCommand processCommand = new ProcessCommand();
        processCommand.process("help");
        Scanner sc = new Scanner(System.in);
        while ((command = sc.nextLine()) != null) {

            if (command.equals("exit")) {
                System.out.println("Closing Database...");
                break;
            }
            processCommand.process(command);
        }
    }
}
