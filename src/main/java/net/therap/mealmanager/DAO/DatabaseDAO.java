package net.therap.mealmanager.DAO;

import net.therap.mealmanager.Connector.Database;
import net.therap.mealmanager.Objects.FoodItems;
import net.therap.mealmanager.Objects.MealItems;
import net.therap.mealmanager.Objects.WeeklyMealPlan;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by Al-Amin on 11/15/2016.
 */
public class DatabaseDAO {
    private boolean isAdmin = false;
    private Connection connection;

    public DatabaseDAO() throws SQLException, IOException {
        Database db = new Database();
        connection = db.connect();

    }

    public void setAdminAccess(boolean flag) {
        isAdmin = flag;
    }

    public boolean getAdminAccess() {
        return isAdmin;
    }

    public <T extends Object> ArrayList<T> query(String[] command) throws SQLException {
        ArrayList<T> object = null;
        if (command[0].equals("weekly_meal_plan")) {
            WeeklyMealPlan entity = new WeeklyMealPlan();
            String cmd = entity.getQuery(command);
            object = (ArrayList<T>) entity.set(executeQuery(cmd));
        }
        if (command[0].equals("meal_items")) {
            MealItems entity = new MealItems();
            String cmd = entity.getQuery(command);
            object = (ArrayList<T>) entity.set(executeQuery(cmd));
        }
        if (command[0].equals("food_items")) {
            FoodItems entity = new FoodItems();
            String cmd = entity.getQuery(command);
            System.out.println(cmd);
            object = (ArrayList<T>) entity.set(executeQuery(cmd));
        }
        return object;
    }

    private ResultSet executeQuery(String command) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(command);
        return resultSet;
    }

    public ResultSet query(String command) throws SQLException {
        Statement statement = connection.createStatement();
        return statement.executeQuery(command);
    }

    public void update(String command) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate(command);
    }

    public void delete(String command) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate(command);
    }

    public void insert(String command) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute(command);
    }
}
