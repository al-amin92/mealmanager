package net.therap.mealmanager.Objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author al-amin
 * @since 11/15/16
 */
public class FoodItems {
    private int item_id;
    private String item_name;
    private int item_cost;
    private String statement;

    public String getQuery(String command[]) {
        String root = "select * from meal_planner.food_items";
        setStatement(root);
        return root;
    }

    public ArrayList<FoodItems> set(ResultSet resultSet) throws SQLException {
        ArrayList<FoodItems> objects = new ArrayList<>();
        while (resultSet.next()) {
            FoodItems foodItems = new FoodItems();
            foodItems.setItem_id(resultSet.getInt("item_id"));
            foodItems.setItem_name(resultSet.getString("item_name"));
            foodItems.setItem_cost(resultSet.getInt("item_cost"));
            objects.add(foodItems);
        }
        return objects;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }


    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public int getItem_cost() {
        return item_cost;
    }

    public void setItem_cost(int item_cost) {
        this.item_cost = item_cost;
    }
}
