package net.therap.mealmanager.Objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author al-amin
 * @since 11/15/16
 */
public class MealItems {
    private int meal_id;
    private int item_id;
    private String item_name;
    private String statement;


    public String getQuery(String command[]) {
        String root = "select * from meal_planner.meal_items";
        if (command.length == 2) {
            root += " where meal_id=" + command[1];
        }
        setStatement(root);
        return root;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public ArrayList<MealItems> set(ResultSet resultSet) throws SQLException {
        ArrayList<MealItems> objects = new ArrayList<>();
        while (resultSet.next()) {
            MealItems mealItems = new MealItems();
            mealItems.setMeal_id(resultSet.getInt("meal_id"));
            mealItems.setItem_id(resultSet.getInt("item_id"));
            mealItems.setItem_name(resultSet.getString("item_name"));
            objects.add(mealItems);
        }
        return objects;
    }

    public int getMeal_id() {
        return meal_id;
    }

    public void setMeal_id(int meal_id) {
        this.meal_id = meal_id;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }
}
