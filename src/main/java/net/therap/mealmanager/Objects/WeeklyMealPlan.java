package net.therap.mealmanager.Objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author al-amin
 * @since 11/15/16
 */
public class WeeklyMealPlan {
    private int meal_id;
    private String week_day;
    private String meal_type;
    private int per_meal_cost;
    private String statement;

    public String getQuery(String command[]) {
        String root = "select * from meal_planner.weekly_meal_plan";

        if (command.length == 2) {
            String prefixDay = command[1].substring(0, 3).toLowerCase();
            root += " where week_day like '" + prefixDay + "%'";
        } else if (command.length == 3) {
            String prefixDay = command[1].substring(0, 3).toLowerCase();
            String prefixTime = command[2].substring(0, 3).toLowerCase();
            root += " where week_day like '" + prefixDay + "%' and meal_type like '" + prefixTime + "%'";
        }
        setStatement(root);
        return root;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public ArrayList<WeeklyMealPlan> set(ResultSet resultSet) throws SQLException {
        ArrayList<WeeklyMealPlan> objects = new ArrayList<>();
        while (resultSet.next()) {
            WeeklyMealPlan weeklyMealPlan = new WeeklyMealPlan();
            weeklyMealPlan.setMeal_id(resultSet.getInt("meal_id"));
            weeklyMealPlan.setWeek_day(resultSet.getString("week_day"));
            weeklyMealPlan.setMeal_type(resultSet.getString("meal_type"));
            weeklyMealPlan.setPer_meal_cost(resultSet.getInt("per_meal_cost"));
            objects.add(weeklyMealPlan);
        }
        return objects;
    }

    public int getMeal_id() {
        return meal_id;
    }

    public void setMeal_id(int meal_id) {
        this.meal_id = meal_id;
    }

    public String getWeek_day() {
        return week_day;
    }

    public void setWeek_day(String week_day) {
        this.week_day = week_day;
    }

    public String getMeal_type() {
        return meal_type;
    }

    public void setMeal_type(String meal_type) {
        this.meal_type = meal_type;
    }

    public int getPer_meal_cost() {
        return per_meal_cost;
    }

    public void setPer_meal_cost(int per_meal_cost) {
        this.per_meal_cost = per_meal_cost;
    }
}
