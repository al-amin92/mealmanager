package net.therap.mealmanager.View;

import net.therap.mealmanager.Objects.FoodItems;
import net.therap.mealmanager.Objects.MealItems;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Al-Amin on 11/15/2016.
 */
public class Show {
    public void showHelp(boolean isAdmin) {
        System.out.printf("%-40s : %s\n", "help", "$ help");
        System.out.printf("%-40s : %s\n", "exit", "$ exit");
        System.out.printf("%-40s : %s\n", "show all food items", "$ show items");
        System.out.printf("%-40s : %s\n", "show meal items for a specific meal", "$ show meal sun lunch");
        System.out.printf("%-40s : %s\n", "show meal properties", "$ show properties");

        if (isAdmin) {
            System.out.printf("%-40s : %s\n", "set item 1,2 and 3 for tuesday lunch", "$ set meal tue lunch 1 2 3");
            System.out.printf("%-40s : %s\n", "insert item 4 and 6 for tuesday lunch", "$ insert meal tue lunch 4 6");
            System.out.printf("%-40s : %s\n", "remove item 2 for tuesday lunch", "$ delete meal tue lunch 2");
            System.out.printf("%-40s : %s\n", "insert an item to the item list",
                    "$ insert item <item_name> <item_cost>");
            System.out.printf("%-40s : %s\n", "delete an item from the item list", "$ delete item <item_id>");
            System.out.printf("%-40s : %s\n", "admin logout", "$ logout");
        } else {
            System.out.printf("%-40s : %s\n", "admin login", "$ login <username> <password>");
        }
    }

    public void showResultSet(ResultSet resultSet, String attributes[]) throws SQLException {
        while (resultSet.next()) {
            for (int i = 0; i < attributes.length; i++) {
                System.out.printf("%-10s | ", resultSet.getString(attributes[i]));
            }
            System.out.println();
        }
    }

    public void printLine(String line) {
        System.out.println(line);
    }

    public void showInfo(ResultSet resultSet2, String[] strings) {

    }

    public void showFoodItems(List<FoodItems> objects, String[] strings) throws NoSuchFieldException {
        System.out.printf("%-30s | %-30s | %-30s\n", "item id", "item name", "item cost(in tk.)");
        String format = "-------------------------------";
        System.out.printf("%-30s--%-30s--%-30s\n", format, format, format);
        for (FoodItems object : objects) {
            System.out.printf("%-30s | %-30s | %-30s\n", object.getItem_id(), object.getItem_name(),
                    object.getItem_cost());
        }
    }

    public void showMealItems(List<MealItems> objects, String[] strings) throws NoSuchFieldException {
        System.out.printf("%-30s | %-30s\n", "item id", "item name");
        String format = "-------------------------------";
        System.out.printf("%-30s--%-30s\n", format, format, format);
        for (MealItems object : objects) {
            System.out.printf("%-30s | %-30s\n", object.getItem_id(), object.getItem_name());
        }
    }

}
